Simple Collaborative Filter
===========================

Description
-----------

Simple Collaborative Filter is an easy way for sites to curate content using collaborative filtering. "Collaborative filtering" is basically creating content recommendations based on a "users who viewed this node also viewed..." approach. This module provides a "simple" way to do this that doesn't make site owners install any heavy dependencies to compute the recommendation algorithm. It works straight out of the box.

The second thing this module does is make content recommendations even before a new piece of content has been viewed. It does this by looking at similar taxonomy terms and content popularity. Collaborative filtering for content-driven sites - e.g., blogs and news outlets - faces a "user zero" problem: until a new piece of content has been viewed by tens of users who also view other pages, there is no content to recommend.

Simple Collaborative Filter provides a Views view that uses a custom contextual filter handler to add extra fields onto each result, which are then used to sort content in a configurable order. Site owners can use this to make content recommendations by configuring the options and the sort order as they see fit.

This module is ideal for curating content for infinite scroll, or for a "Users who viewed this node also viewed" recommendation block.

See "configuration" below for a more detailed explanation.


Similar modules
---------------

Browsing History Recommender (https://www.drupal.org/project/history_rec) - uses the Recommender API's algorithms to prepare recommendations based on browsing history.


Dependencies
------------

Focused History module (https://www.drupal.org/project/focused_history)
Taxonomy module (Drupal core)
Views module (https://www.drupal.org/project/views)

7.x-1.x relies on the Focused History module, which allows a site to store content-viewing history in a more configurable manner than the normal `history` and `accesslog` tables.


Installation
------------

Nothing special about installation. The normal Drupal way:

1) Download Simple Collaborative Filter to your modules directory.
2) Enable Simple Collaborative Filter in administer >> modules.


Configuration
-------------

1) You'll want to first make sure to configure Focused History to collect content-viewing history that makes sense for your site: /admin/config/system/focused_history
2) Configure the Simple Collaborative Filter view at admin/structure/views/view/simple_cf/edit

There are a number of configurations that can be made. See INSTALL.txt.


View Displays
-------------

This module provides 3 views displays out of box:

1) Admin Block - This is intended for admins to be able to view outputs to configure other Views. This is where you'll enter a nid to preview how this works with the contextual filter options you've selected.

2) Titles Block - Fully filtered and sorted block with content titles only (linked to content). Ideal for a related content block.

3) Node IDs Block - Fully filtered and sorted block with an HTML list of nids only. Ideal for rendering nodes to serve via ajax in infinite scroll.
