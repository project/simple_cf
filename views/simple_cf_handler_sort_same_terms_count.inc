<?php

/**
 * @file
 * Defines the Simple Collaborative Filter sort handler for simple_cf_same_terms_count.
 */

/**
 * Defines the simple_cf_handler_sort_same_terms_count sort handler.
 */
class simple_cf_handler_sort_same_terms_count extends views_handler_sort {

  /**
   * Defines default options.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['order'] = array('default' => 'DESC');
    return $options;
  }

  /**
   * Defines query elements.
   */
  public function query() {
    if (in_array('simple_cf', $this->query->tags)) {
      $this->query->add_orderby(NULL, NULL, $this->options['order'], 'simple_cf_same_terms_count');
    }
  }

}
