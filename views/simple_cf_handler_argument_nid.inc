<?php

/**
 * @file
 * Defines the node ID argument for Simple Collaborative Filter
 */

/**
 * Contextual filter handler for the node ID argument for Simple Collaborative Filter
 */
class simple_cf_handler_argument_nid extends views_handler_argument_numeric {

  /**
   * Defines default values for argument settings.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['simple_cf_viewed_together_threshold'] = array('default' => 1);
    $options['simple_cf_taxonomy_terms'] = array('default' => array());
    return $options;
  }

  /**
   * Defines the options form.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['simple_cf'] = array(
      '#type' => 'fieldset',
      '#title' => t('Simple Collaborative Filter options'),
    );
    $form['simple_cf_viewed_together_threshold'] = array(
      '#type' => 'select',
      '#title' => t('Viewed together threshold'),
      '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)),
      '#default_value' => $this->options['simple_cf_viewed_together_threshold'],
      '#description' => t('Threshold for number of times content has been viewed together.'),
      '#fieldset' => 'simple_cf',
    );

    $vocabulary_options = db_query('SELECT vid, name FROM taxonomy_vocabulary')->fetchAllKeyed();
    $form['simple_cf_taxonomy_terms'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Same Term Vocabularies'),
      '#options' => $vocabulary_options,
      '#default_value' => $this->options['simple_cf_taxonomy_terms'],
      '#description' => t('Vocabularies to include for calculating a same terms count.'),
      '#fieldset' => 'simple_cf',
    );

  }

  /**
   * Overrides argument's default actions list to ensure that node ID
   * from URL is provided.
   */
  // public function default_actions($which = NULL) {
  //   $defaults = parent::default_actions();
  //   $defaults_keys = array_keys($defaults);
  //   foreach ($defaults as $key => $default) {
  //     if ($key !== 'default') {
  //       unset($defaults[$key]);
  //     }
  //   }

  //   if ($which) {
  //     if (!empty($defaults[$which])) {
  //       return $defaults[$which];
  //     }
  //   }
  //   else {
  //     return $defaults;
  //   }
  // }

  /**
   * Builds the query.
   */
  public function query($group_by = FALSE) {

    $this->ensure_my_table();

    // Join counts from focused history table
    $sids = db_query(
      "SELECT sid
        FROM focused_history
        WHERE nid IN (:nids)", array(':nids' => $this->value))->fetchCol();

    $subquery = db_select('focused_history');
    $subquery->addField('focused_history', 'nid');
    $subquery->addExpression('count(nid)', 'total_views_count');

    // Only add viewed together count expression if there are sids that have viewed this node.
    if (!empty($sids)) {
      $sids_string = implode(',', $sids);
      $expression = sprintf('SUM(CASE WHEN sid IN (%s) THEN 1 END)', $sids_string);
      $subquery->addExpression($expression, 'viewed_together_count');
    }

    $subquery->groupBy('nid');

    $join = new views_join();
    $join->definition = array('table formula' => $subquery, 'left_field' => 'nid', 'field' => 'nid', 'left_table' => 'node');
    $join->field = 'nid';
    $join->left_table = 'node';
    $join->left_field = 'nid';
    $join->type = 'left';
    $this->query->add_relationship('focused_history_aggregates', $join, 'node');

    // Add total views count field
    $this->query->add_field('focused_history_aggregates', 'total_views_count', 'simple_cf_total_views_count');

    // Add viewed together count field and viewed together score field
    if (!empty($sids)) {
      $this->query->add_field('focused_history_aggregates', 'viewed_together_count', 'simple_cf_viewed_together_count');

      $expression = sprintf('CASE WHEN focused_history_aggregates.viewed_together_count >= %d THEN focused_history_aggregates.viewed_together_count ELSE 0 END', $this->options['simple_cf_viewed_together_threshold']);
      $this->query->add_field('', $expression, 'simple_cf_viewed_together_score');
    }
    else {
      $this->query->add_field('', '0', 'simple_cf_viewed_together_count');
      $this->query->add_field('', '0', 'simple_cf_viewed_together_score');
    }

    // Join count from taxonomy index table
    if (!empty($this->options['simple_cf_taxonomy_terms'])) {
      $tids = db_query(
        "SELECT ti.tid
          FROM taxonomy_index ti
          JOIN taxonomy_term_data td
          ON ti.tid = td.tid
          WHERE ti.nid IN (:nids)
          AND td.vid IN (:vids)",
        array(':nids' => $this->value, ':vids' => $this->options['simple_cf_taxonomy_terms']))->fetchCol();

      if (!empty($tids)) {
        $subquery = db_select('taxonomy_index');
        $subquery->condition('tid', $tids, 'IN');
        $subquery->addField('taxonomy_index', 'nid');
        $subquery->addExpression('count(tid)', 'count');
        $subquery->groupBy('nid');

        $join = new views_join();
        $join->definition = array('table formula' => $subquery, 'left_field' => 'nid', 'field' => 'nid', 'left_table' => 'node');
        $join->field = 'nid';
        $join->left_table = 'node';
        $join->left_field = 'nid';
        $join->type = 'left';
        $this->query->add_relationship('simple_cf_same_terms', $join, 'node');
        $this->query->add_field('simple_cf_same_terms', 'count', 'simple_cf_same_terms_count');

      }
      else {
        $this->query->add_field('', '0', 'simple_cf_same_terms_count');
      }
    }
    else {
      $this->query->add_field('', '0', 'simple_cf_same_terms_count');
    }

    // Exclude the current node.
    $this->query->add_where(0, 'node.nid', $this->value, 'NOT IN');

    // Add a tag to indicate that filters and sort handlers are valid.
    $this->query->add_tag('simple_cf');
  }

}
