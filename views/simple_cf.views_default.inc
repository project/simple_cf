<?php

/**
 * Views for Simple Collaborative Filter.
 */

/**
 * Implements hook_views_default_views()
 */
function simple_cf_views_default_views() {
  $view = new view();
  $view->name = 'simple_cf';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Simple Collaborative Filter';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Simple Collaborative Filter';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Simple Collaborative Filter: Viewed Together Count */
  $handler->display->display_options['sorts']['viewed_together_count']['id'] = 'viewed_together_count';
  $handler->display->display_options['sorts']['viewed_together_count']['table'] = 'simple_cf';
  $handler->display->display_options['sorts']['viewed_together_count']['field'] = 'viewed_together_count';
  /* Sort criterion: Simple Collaborative Filter: Same Terms Count */
  $handler->display->display_options['sorts']['same_terms_count']['id'] = 'same_terms_count';
  $handler->display->display_options['sorts']['same_terms_count']['table'] = 'simple_cf';
  $handler->display->display_options['sorts']['same_terms_count']['field'] = 'same_terms_count';
  /* Sort criterion: Simple Collaborative Filter: Total Views Count */
  $handler->display->display_options['sorts']['total_views_count']['id'] = 'total_views_count';
  $handler->display->display_options['sorts']['total_views_count']['table'] = 'simple_cf';
  $handler->display->display_options['sorts']['total_views_count']['field'] = 'total_views_count';
  /* Contextual filter: Simple Collaborative Filter: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'simple_cf';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';

  /* Display: Admin Block */
  $handler = $view->new_display('block', 'Admin Block', 'block_admin');
  $handler->display->display_options['display_description'] = 'This is intended for admins to be able to view outputs to configure other Views.';
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer simple collaborative filter';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'same_terms_count' => 'same_terms_count',
    'total_views_count' => 'total_views_count',
    'viewed_together_count' => 'viewed_together_count',
  );
  $handler->display->display_options['style_options']['default'] = 'viewed_together_count';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'same_terms_count' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'total_views_count' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'viewed_together_count' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Simple Collaborative Filter: Viewed Together Count */
  $handler->display->display_options['fields']['viewed_together_count']['id'] = 'viewed_together_count';
  $handler->display->display_options['fields']['viewed_together_count']['table'] = 'simple_cf';
  $handler->display->display_options['fields']['viewed_together_count']['field'] = 'viewed_together_count';
  /* Field: Simple Collaborative Filter: Same Terms Count */
  $handler->display->display_options['fields']['same_terms_count']['id'] = 'same_terms_count';
  $handler->display->display_options['fields']['same_terms_count']['table'] = 'simple_cf';
  $handler->display->display_options['fields']['same_terms_count']['field'] = 'same_terms_count';
  /* Field: Simple Collaborative Filter: Total Views Count */
  $handler->display->display_options['fields']['total_views_count']['id'] = 'total_views_count';
  $handler->display->display_options['fields']['total_views_count']['table'] = 'simple_cf';
  $handler->display->display_options['fields']['total_views_count']['field'] = 'total_views_count';
  $handler->display->display_options['block_description'] = 'admin block';

  /* Display: Titles Block */
  $handler = $view->new_display('block', 'Titles Block', 'block_titles');
  $handler->display->display_options['display_description'] = 'Fully filtered and sorted block with content titles only. Ideal for a related content block.';
  $handler->display->display_options['block_description'] = 'titles block';

  /* Display: Node IDs Block */
  $handler = $view->new_display('block', 'Node IDs Block', 'block_nids');
  $handler->display->display_options['display_description'] = 'Fully filtered and sorted block with nids only. Ideal for rendering nodes to serve via ajax.';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['element_type'] = '0';
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nid']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['nid']['element_default_classes'] = FALSE;
  $handler->display->display_options['block_description'] = 'Node IDs';

  $views[$view->name] = $view;
  return $views;
}
