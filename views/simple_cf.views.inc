<?php

/**
 * @file
 * View hook implementations for Simple Collaborative Filter module.
 */

/**
 * Implements hook_views_data().
 */
function simple_cf_views_data() {
  $data = array();

  $data['simple_cf']['table']['group'] = t('Simple Collaborative Filter');
  $data['simple_cf']['table']['join'] = array('#global' => array());

  $data['simple_cf']['viewed_together_count'] = array(
    'title' => t('Viewed Together Count'),
    'help' => t('The number of times a node has been viewed by users who have also viewed the current node.'),
    'field' => array(
      'handler' => 'simple_cf_handler_field_viewed_together_count',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'simple_cf_handler_sort_viewed_together_count',
    ),
  );
  $data['simple_cf']['total_views_count'] = array(
    'title' => t('Total Views Count'),
    'help' => t('The number of times this node has been viewed.'),
    'field' => array(
      'handler' => 'simple_cf_handler_field_total_views_count',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'simple_cf_handler_sort_total_views_count',
    ),
  );
  $data['simple_cf']['same_terms_count'] = array(
    'title' => t('Same Terms Count'),
    'help' => t('The number of terms this node has in common with another node.'),
    'field' => array(
      'handler' => 'simple_cf_handler_field_same_terms_count',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'simple_cf_handler_sort_same_terms_count',
    ),
  );
  $data['simple_cf']['nid'] = array(
    'title' => t('Nid'),
    'help' => t('Nid of content. Passes content IDs to Simple Collaborative Filter.'),
    'argument' => array(
      'handler' => 'simple_cf_handler_argument_nid',
    ),
  );

  return $data;
}
