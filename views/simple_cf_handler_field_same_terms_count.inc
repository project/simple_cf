<?php

/**
 * @file
 * Provides a field for displaying the total number of terms a node has in common with another.
 */

/**
 * Same terms count field handler.
 */
class simple_cf_handler_field_same_terms_count extends views_handler_field {

  /**
   * Option definition.
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * Options form for selecting between number and percentage.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  /**
   * Enables click sortable functionality.
   */
  function click_sort($order) {
    $this->query->orderby[] = array(
      'field' => 'simple_cf_same_terms_count',
      'direction' => strtoupper($order),
    );
  }

  function query() {}

  /**
   * Renders the number of terms this node has in common with another node.
   */
  function render($data) {
    if ($data->simple_cf_same_terms_count) {
      return $data->simple_cf_same_terms_count;
    }
    return 0;
  }

}
