<?php

/**
 * @file
 * Provides a field for displaying the number of users who have viewed other nodes in addition to this one.
 */

/**
 * Viewed together count field handler.
 */
class simple_cf_handler_field_viewed_together_count extends views_handler_field {

  /**
   * Option definition.
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * Options form for selecting between number and percentage.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  /**
   * Enables click sortable functionality.
   */
  function click_sort($order) {
    $this->query->orderby[] = array(
      'field' => 'simple_cf_viewed_together_count',
      'direction' => strtoupper($order),
    );
  }

  function query() {}

  /**
   * Renders the number of times this node was viewed together with the node argument.
   */
  function render($data) {
    if ($data->simple_cf_viewed_together_count) {
      return $data->simple_cf_viewed_together_count;
    }
    return 0;
  }

}
