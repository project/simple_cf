<?php

/**
 * @file
 * Provides a field for displaying the total number of times a node has been viewed.
 */

/**
 * Totals views count field handler.
 */
class simple_cf_handler_field_total_views_count extends views_handler_field {

  /**
   * Option definition.
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * Options form for selecting between number and percentage.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  /**
   * Enables click sortable functionality.
   */
  function click_sort($order) {
    $this->query->orderby[] = array(
      'field' => 'simple_cf_total_views_count',
      'direction' => strtoupper($order),
    );
  }

  function query() {}

  /**
   * Renders the number of times this node was viewed.
   */
  function render($data) {
    if ($data->simple_cf_total_views_count) {
      return $data->simple_cf_total_views_count;
    }
    return 0;
  }

}
